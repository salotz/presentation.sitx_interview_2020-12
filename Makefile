.PHONY: all split_slides

all: presentation.pdf presentation.squeezed.pdf

clean:
	rm -f presentation.pdf
	rm -rf slides

presentation.pdf: presentation.svg
	inkscape_pages $^

# compress the file
presentation.squeezed.pdf: presentation.pdf
	cpdfsqueeze $^ $@

slides:
	mkdir -p $@

slides/slide_%.pdf: slides presentation.pdf
	cpdf -split $^ -o slides/slide_%%%.pdf

split_slides: slides/slide_%.pdf
